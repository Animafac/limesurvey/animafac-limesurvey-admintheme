# animafac-limesurvey-admintheme

LimeSurvey admin theme used on https://forms.animafac.net/

## Install

This theme needs to be install in the `upload/admintheme/` folder of your LimeSurvey instance and then enabled in the LimeSurvey admin.
